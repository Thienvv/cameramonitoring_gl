#ifndef STDCOMMON_H
#define STDCOMMON_H

// Standard C++
#include <fstream>
#include <string>

// QT
#include <QDialog>
#include <QString>
#include <QWidget>
#include <QSpinBox>
#include <QLabel>
#include <QMenuBar>
#include <QToolBar>
#include <QAction>
#include <QDateTime>

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QStatusBar>
#include <QGraphicsPixmapItem>
#include <QMap>
#include <QTimer>
#include <QGridLayout>

#include <QFileDialog>
#include <QMessageBox>



// Project
// #include "Defines/Definition.h"
#include "Utilities/inipp.h"
#include "Utilities/TypeConversion.h"
#endif // STDCOMMON_H
