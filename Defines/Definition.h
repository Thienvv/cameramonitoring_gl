#ifndef DEFINITION_H
#define DEFINITION_H

#include <stdCommon.h>
using namespace std;

// 2021.04.25 by Thienvv [MOD] Change order
enum EnColor:int
{
    COLOR_WHITE,
    COLOR_BLACK,
    COLOR_YELLOW,
    COLOR_BLUE,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_MAX
};

enum EnDioInput:int
{
    // BCM  => wPi
    DI_GPIO_17  = 0, // BCM
    DI_GPIO_18  = 1, // BCM

    DI_GPIO_26  = 25,
    DI_GPIO_20  = 28,
    DI_GPIO_21  = 29,
};


class Definition
{
private:
    Definition();

public:
    static QString _VERSION;

    static string _SLASH;
    static string _MACHINE_NAME;
    static string _FOLDER_ROOT;
    static string _FOLDER_CONFIG;
    static string _FOLDER_RECIPE;
    static string _FOLDER_WORK_INFO;
    static string _FOLDER_LOG;
    static string _FILE_DEFAULT_RECIPE_NAME;
    static string _FILE_CONFIG_TYPE;

    static int IMG_WIDTH;
    static int IMG_HEIGHT;
    static int NUMBER_OF_BOARD_SETVALUES;
};

#endif // DEFINITION_H
