#include "Definition.h"

Definition::Definition()
{

}

#ifdef WIN32
QString Definition::_VERSION = "Version 1.0.2012";
string Definition::_SLASH = "\\";
string Definition::_MACHINE_NAME = "METAL DETECTOR";
string Definition::_FOLDER_ROOT = "D:\\IRONMAN\\" + _MACHINE_NAME + _SLASH;


#elif __linux__

QString Definition::_VERSION = "Version 1.0.2012";

string Definition::_SLASH = "/";
string Definition::_MACHINE_NAME = "GLASS_MONITORING";
string Definition::_FOLDER_ROOT = "/home/pi/Thienvv/Projects/PROGRAM/GlassMonitoring/Data/";
//string Definition::_FOLDER_ROOT = "/home/ironman/Thienvv/MDM/IRONMAN/" + _MACHINE_NAME + _SLASH;
#endif

string Definition::_FOLDER_CONFIG = _FOLDER_ROOT +  "CONFIG"+ _SLASH;
string Definition::_FOLDER_RECIPE = _FOLDER_ROOT + "RECIPES"+ _SLASH;
string Definition::_FOLDER_WORK_INFO = _FOLDER_ROOT + "RESULT_IMAGES"+ _SLASH;
string Definition::_FOLDER_LOG = _FOLDER_ROOT + "LOG" + _SLASH;

//string Definition::_FILE_DEFAULT_RECIPE_NAME = "DEFAULT";

string Definition::_FILE_CONFIG_TYPE = ".ini";



int Definition::IMG_WIDTH = 640;
int Definition::IMG_HEIGHT = 480;
int Definition::NUMBER_OF_BOARD_SETVALUES = 14;

