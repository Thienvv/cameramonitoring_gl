#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <stdCommon.h>
#include <Processes/ProcessMain.h>
#include <UI/SubPanels/SubPanel_Vision.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
   void ButtonClick();
   void EventTimerTick();

   // Methods
private:
       void InitializeVariables();
       void ConnectSlots();
       void AssignPanel();

       void InitializeUIControls();

       void ExitProgram();

private:
    Ui::MainWindow *ui;

    SubPanel_Vision *panel_Teaching;

    QTimer *timer30ms;


};

#endif // MAINWINDOW_H
