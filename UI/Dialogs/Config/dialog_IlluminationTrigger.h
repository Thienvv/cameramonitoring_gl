#ifndef DIALOG_ILLUMINATIONTRIGGER_H
#define DIALOG_ILLUMINATIONTRIGGER_H

#include <QDialog>
#include <Processes/ProcessVision.h>
#include <Processes/ProcessMain.h>
#include <Defines/Definition.h>

namespace Ui {
class Dialog_IlluminationTrigger;
}

class Dialog_IlluminationTrigger : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_IlluminationTrigger(QWidget *parent = nullptr);
    ~Dialog_IlluminationTrigger();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_clicked();

private:
    Ui::Dialog_IlluminationTrigger *ui;
    ProcessVision *m_procVision;

    void InitializeVariable();
    void InitializeSlots();

    bool ApplyParam();

    int GetDigitalInputEnum();
	
public:
    bool DisplayUI();
};

#endif // DIALOG_ILLUMINATIONTRIGGER_H
