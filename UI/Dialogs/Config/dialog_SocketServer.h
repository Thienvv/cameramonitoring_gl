#ifndef DIALOG_SOCKETSERVER_H
#define DIALOG_SOCKETSERVER_H

#include <QDialog>
#include <Processes/ProcessMain.h>
#include <Processes/ProcessVision.h>

#include <Processes/Socket/qtcustomserver.h>

namespace Ui {
class Dialog_SocketServer;
}

class Dialog_SocketServer : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_SocketServer(QWidget *parent = nullptr);
    ~Dialog_SocketServer();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_clicked();

private:
    Ui::Dialog_SocketServer *ui;

    void InitializeVariable();
    void InitializeSlots();

    bool ApplyParam();

public:
    bool DisplayUI();

private:
//    QtCustomServer m_myServer;
};

#endif // DIALOG_SOCKETSERVER_H
