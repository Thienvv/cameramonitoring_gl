#include "dialog_SocketServer.h"
#include "ui_dialog_SocketServer.h"

Dialog_SocketServer::Dialog_SocketServer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_SocketServer)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::CustomizeWindowHint);
    this->setWindowFlags(Qt::Dialog);
    this->setFixedSize(this->width(),this->height());

    InitializeVariable();
    InitializeSlots();
}

Dialog_SocketServer::~Dialog_SocketServer()
{
    delete ui;
}

void Dialog_SocketServer::InitializeVariable()
{
}

void Dialog_SocketServer::InitializeSlots()
{
    connect(ui->btn_Apply, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
    connect(ui->btn_Save, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
    connect(ui->btn_Close, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
}

void Dialog_SocketServer::on_buttonBox_accepted()
{
//    ApplyParam();
}

void Dialog_SocketServer::on_buttonBox_clicked()
{
    QPushButton *btn = (QPushButton *)sender();
    if(btn == ui->btn_Apply)
    {
        ApplyParam();
    }
    else if(btn == ui->btn_Save)
    {
    }
    else if(btn == ui->btn_Close)
    {
        this->hide();
    }
}

// 2021.04.25 by Thienvv [ADD] Refactoring code
bool Dialog_SocketServer::ApplyParam()
{
//    m_myServer.StartServer();
    return true;
}

bool Dialog_SocketServer::DisplayUI()
{

    return true;
}
