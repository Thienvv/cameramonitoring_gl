#include "dialog_IlluminationTrigger.h"
#include "ui_dialog_IlluminationTrigger.h"

Dialog_IlluminationTrigger::Dialog_IlluminationTrigger(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_IlluminationTrigger)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::CustomizeWindowHint);
    this->setWindowFlags(Qt::Dialog);
    this->setFixedSize(this->width(),this->height());

    InitializeVariable();
    InitializeSlots();
}

Dialog_IlluminationTrigger::~Dialog_IlluminationTrigger()
{
    delete ui;
}

void Dialog_IlluminationTrigger::InitializeVariable()
{
    m_procVision = ProcessVision::GetInstance();
	
	ui->comboBox_DI->addItem("GPIO_17");
    ui->comboBox_DI->addItem("GPIO_18");
    ui->comboBox_DI->addItem("GPIO_20");
    ui->comboBox_DI->addItem("GPIO_21");
    ui->comboBox_DI->addItem("GPIO_26");
    ui->comboBox_DI->setCurrentIndex(2);
}

void Dialog_IlluminationTrigger::InitializeSlots()
{
    connect(ui->btn_Apply, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
    connect(ui->btn_Save, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
    connect(ui->btn_Close, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
}

void Dialog_IlluminationTrigger::on_buttonBox_accepted()
{
    // ApplyParam();
}


void Dialog_IlluminationTrigger::on_buttonBox_clicked()
{
    QPushButton *btn = (QPushButton *)sender();
    if(btn == ui->btn_Apply)
    {
        ApplyParam();
    }
    else if(btn == ui->btn_Save)
    {
//        m_procVision->SaveConfigToFile();
        ProcessMain::SaveRecipe();
    }
    else if(btn == ui->btn_Close)
    {
        this->hide();
    }
}

bool Dialog_IlluminationTrigger::ApplyParam()
{
    m_procVision->m_nDelayAfterTrigger = ui->spinBox_DelayViewAfterTrigger->value();
    m_procVision->m_bSaveImageAfterTrigger = ui->checkBox_SaveImageAfterTrigger->isChecked();
    m_procVision->m_Illumination_Channel_1 = ui->spinBox_Illumination_CH1->value();
    m_procVision->m_Illumination_Channel_2 = ui->spinBox_Illumination_CH2->value();

    int nDiIndex = GetDigitalInputEnum();
    m_procVision->m_bActiveHigh = ui->checkBox_ActiveHigh->isChecked();
    // m_procVision->m_nIndexDigitalInputForDelay = ui->comboBox_DI->currentIndex();
    m_procVision->m_nIndexDigitalInputForDelay = nDiIndex; // 2021.06.02
//    ProcessBoardControl::GetInstance()->SetPinModeInput(enDiIndex);

    return true;
}

bool Dialog_IlluminationTrigger::DisplayUI()
{
    //    DI_GPIO_17  = 0, // BCM
    //    DI_GPIO_18  = 1, // BCM

    //    DI_GPIO_20  = 28,
    //    DI_GPIO_21  = 29,
    //    DI_GPIO_26  = 25,
//    ui->comboBox_DI->addItem("GPIO_17");
//    ui->comboBox_DI->addItem("GPIO_18");
//    ui->comboBox_DI->addItem("GPIO_20");
//    ui->comboBox_DI->addItem("GPIO_21");
//    ui->comboBox_DI->addItem("GPIO_26");
    int nComboIndex = 2;
    switch (m_procVision->m_nIndexDigitalInputForDelay) {
    case 0:
        nComboIndex = 0;
        break;
    case 1:
        nComboIndex = 1;
        break;
    case 25:
        nComboIndex = 4;
        break;
    case 28:
        nComboIndex = 2;
        break;
    case 29:
        nComboIndex = 3;
        break;
    default:
        nComboIndex = 2;
        break;
    }
    ui->comboBox_DI->setCurrentIndex(nComboIndex) ;


    ui->checkBox_ActiveHigh->setChecked(m_procVision->m_bActiveHigh );

    ui->spinBox_DelayViewAfterTrigger->setValue(m_procVision->m_nDelayAfterTrigger);
    ui->checkBox_SaveImageAfterTrigger->setChecked(m_procVision->m_bSaveImageAfterTrigger);

    ui->spinBox_Illumination_CH1->setValue(m_procVision->m_Illumination_Channel_1);
    ui->spinBox_Illumination_CH2->setValue(m_procVision->m_Illumination_Channel_2);

    return true;
}

int Dialog_IlluminationTrigger::GetDigitalInputEnum()
{
    switch(ui->comboBox_DI->currentIndex())
    {
    case 0:
        return DI_GPIO_17;
    case 1:
        return DI_GPIO_18;
    case 2:
        return DI_GPIO_20;
    case 3:
        return DI_GPIO_21;
    case 4:
        return DI_GPIO_26;
    default:
        return DI_GPIO_20;
    }
}
