#ifndef DIALOG_GRIDLINECONFIG_H
#define DIALOG_GRIDLINECONFIG_H

#include <QDialog>
#include <Processes/ProcessMain.h>
#include <Processes/ProcessVision.h>

namespace Ui {
class Dialog_GridLineConfig;
}

class Dialog_GridLineConfig : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_GridLineConfig(QWidget *parent = nullptr);
    ~Dialog_GridLineConfig();

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_clicked();

private:
    Ui::Dialog_GridLineConfig *ui;
    ProcessVision *m_procVision;

    void InitializeVariable();
    void InitializeSlots();

    bool ApplyParam();

public:
    bool DisplayUI();
};

#endif // DIALOG_GRIDLINECONFIG_H
