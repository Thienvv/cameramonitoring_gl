#include "dialog_gridlineconfig.h"
#include "ui_dialog_gridlineconfig.h"

Dialog_GridLineConfig::Dialog_GridLineConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_GridLineConfig)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::CustomizeWindowHint);
    this->setWindowFlags(Qt::Dialog);
    this->setFixedSize(this->width(),this->height());

    InitializeVariable();
    InitializeSlots();
}

Dialog_GridLineConfig::~Dialog_GridLineConfig()
{
    delete ui;
}

void Dialog_GridLineConfig::InitializeVariable()
{
    m_procVision = ProcessVision::GetInstance();

    // Initialize for combo box
    // 2021.04.25 by Thienvv [MOD] Change order
    ui->comboBox_CrossColor->addItem("WHITE");
    ui->comboBox_CrossColor->addItem("BLACK");
    ui->comboBox_CrossColor->addItem("YELLOW");
    ui->comboBox_CrossColor->addItem("BLUE");
    ui->comboBox_CrossColor->addItem("RED");
    ui->comboBox_CrossColor->addItem("GREEN");

    ui->comboBox_GridColor->addItem("WHITE");
    ui->comboBox_GridColor->addItem("BLACK");
    ui->comboBox_GridColor->addItem("YELLOW");
    ui->comboBox_GridColor->addItem("BLUE");
    ui->comboBox_GridColor->addItem("RED");
    ui->comboBox_GridColor->addItem("GREEN");
}

void Dialog_GridLineConfig::InitializeSlots()
{
    connect(ui->btn_Apply, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
    connect(ui->btn_Save, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
    connect(ui->btn_Close, SIGNAL(released()), this, SLOT(on_buttonBox_clicked()));
}

void Dialog_GridLineConfig::on_buttonBox_accepted()
{
//    ApplyParam();
}

void Dialog_GridLineConfig::on_buttonBox_clicked()
{
    QPushButton *btn = (QPushButton *)sender();
    if(btn == ui->btn_Apply)
    {
        ApplyParam();
    }
    else if(btn == ui->btn_Save)
    {
//        m_procVision->SaveConfigToFile();
        ProcessMain::SaveRecipe();
    }
    else if(btn == ui->btn_Close)
    {
        this->hide();
    }
}

// 2021.04.25 by Thienvv [ADD] Refactoring code
bool Dialog_GridLineConfig::ApplyParam()
{
    m_procVision->m_bShowGrid = ui->checkBox_Show->isChecked();
    m_procVision->m_nCrossGridPitch =  ui->spinBox_Distance->value();
    m_procVision->m_nCrossGridSize = ui->spinBox_Length->value();
    m_procVision->m_enCrossCrossColor = (EnColor)ui->comboBox_CrossColor->currentIndex();
    m_procVision->m_enCrossGridColor = (EnColor)ui->comboBox_GridColor->currentIndex();
    m_procVision->m_dGridPitchDistance = ui->spinBox_GridUnitSize->value();

    return true;
}

bool Dialog_GridLineConfig::DisplayUI()
{
    ui->checkBox_Show->setChecked(m_procVision->m_bShowGrid);
    ui->spinBox_Distance->setValue(m_procVision->m_nCrossGridPitch );
    ui->spinBox_Length->setValue(m_procVision->m_nCrossGridSize );
    ui->comboBox_CrossColor->setCurrentIndex(m_procVision->m_enCrossCrossColor);
    ui->comboBox_GridColor->setCurrentIndex(m_procVision->m_enCrossGridColor);
    ui->spinBox_GridUnitSize->setValue(m_procVision->m_dGridPitchDistance);

    return true;
}
