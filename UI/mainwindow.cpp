#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    this->setWindowFlags(Qt::FramelessWindowHint);
    this->setFixedSize(this->width(),this->height());

    InitializeVariables();
    AssignPanel();
    ConnectSlots();
    InitializeUIControls();

    ProcessMain::InitProcess();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::InitializeVariables()
{
    panel_Teaching = new SubPanel_Vision(this);
}

void MainWindow::AssignPanel()
{
    ui->panel_Main->removeWidget(ui->page);
    ui->panel_Main->removeWidget(ui->page_2);

    ui->panel_Main->addWidget((QWidget *) panel_Teaching);
}

void MainWindow::ConnectSlots()
{
    connect(ui->btn_Exit, SIGNAL(released()), this, SLOT(ButtonClick()));
}

void MainWindow::InitializeUIControls()
{
    timer30ms =  new QTimer(this);
    connect(timer30ms, SIGNAL(timeout()), this, SLOT(EventTimerTick()));
    timer30ms->start();

    ui->lbl_ProgramVersion->setText("1.0.0.1");
    //panel_Teaching->DisplayUpdateUI();
}

void MainWindow::EventTimerTick()
{
    QString strDtNow = QDate::currentDate().toString(Qt::ISODate) +  " " + QTime::currentTime().toString();
    ui->lbl_CurrentTime->setText(strDtNow);
}

void MainWindow::ButtonClick()
{
    QPushButton *btn = (QPushButton *)sender();

    if(btn == ui->btn_Exit)
    {
        ExitProgram();
    }
}

void MainWindow::ExitProgram()
{
    // Stop Capture image here
    ProcessMain::ExitProcesses();
    delay(5);

    this->close();
}
