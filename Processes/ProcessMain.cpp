#include "ProcessMain.h"

ProcessMain::ProcessMain()
{
    InstProcVision = ProcessVision::GetInstance();
    InstProcRecipe = ProcessRecipe::GetInstance();
}

ProcessVision* ProcessMain::InstProcVision = nullptr;
ProcessRecipe* ProcessMain::InstProcRecipe = nullptr;

void ProcessMain::InitProcess()
{
    InstProcVision = ProcessVision::GetInstance();
    InstProcRecipe = ProcessRecipe::GetInstance();
}
void ProcessMain::ExitProcesses()
{
    InstProcVision->StopThreadAcquisition();
    InstProcVision->StopThreadImageProcessing();
}

bool ProcessMain::LoadRecipe()
{
    if(false == InstProcRecipe->LoadRecipe())
        return false;

    InstProcVision->LoadConfigFromFile(InstProcRecipe->CurrentRecipeDir, InstProcRecipe->CurrentRecipeName);

    return true;
}

bool ProcessMain::SaveRecipe()
{
    if(false == InstProcRecipe->IsCurrentRecipeValid())
        return false;

    InstProcVision->SaveConfigToFile(InstProcRecipe->CurrentRecipeDir, InstProcRecipe->CurrentRecipeName);

    return true;
}
