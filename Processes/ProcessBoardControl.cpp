#include "ProcessBoardControl.h"
#include "QDebug"

// Singleton
// Null, because instance will be initialized on demand.
ProcessBoardControl* ProcessBoardControl::_instance = nullptr;
ProcessBoardControl* ProcessBoardControl::GetInstance()
{
    if(_instance == nullptr)
    {
        _instance = new ProcessBoardControl();
    }
    return _instance;
}

ProcessBoardControl::ProcessBoardControl()
{

}

DIOControl *ProcessBoardControl::m_DioControl = new DIOControl();

PI_THREAD (IOControl)
{
    DIOControl *adsControl = ProcessBoardControl::m_DioControl;
    while(true)
    {
        adsControl->mIsThreadRunningIO = true;
        if(adsControl->mStopThreadIO)
        {
            adsControl->mIsThreadRunningIO = false;
            break;
        }

        switch (adsControl->mCurrStepIO)
        {
        case 0:
            adsControl->mCurrStepIO++;
            break;

        case 1:
            adsControl->mCurrStepIO++;

            //delay (100) ;
            break;

        case 2:
//            adsControl->SetPinOut(LED_1, false);
//            delayMicroseconds(500000);

//            adsControl->SetPinOut(LED_1, true);
//            delayMicroseconds(500000);
            break;

        case 3:
            adsControl->mCurrStepIO= 100;
            break;

        case 100:
            //            printf(" *** %5d ***\n", adsControl->mCurrStep);
            adsControl->mStopThreadIO = true;
            adsControl->mIsThreadRunningIO = false;
            break;
        }
    }
}


void ProcessBoardControl::StartThread()
{
    m_DioControl->StartThread();

    if(ProcessBoardControl::m_DioControl->mIsThreadRunningIO == false)
    {
        piThreadCreate (IOControl);
    }
}

void ProcessBoardControl::StopThread()
{
    m_DioControl->StopThread();
}

bool ProcessBoardControl::SetPinModeInput(int nDioInput)
{
    return m_DioControl->SetPinMode(nDioInput, true);
}

bool ProcessBoardControl::GetDIOInput(EnDioInput enDioInput)
{
    return m_DioControl->GetPinIn(enDioInput);
}

bool ProcessBoardControl::GetDIOInput(int nDioInput)
{
    return m_DioControl->GetPinIn(nDioInput);
}
