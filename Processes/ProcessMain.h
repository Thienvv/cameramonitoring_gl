#ifndef PROCESSMAIN_H
#define PROCESSMAIN_H

#include "Processes/ProcessVision.h"
#include "Processes/ProcessRecipe.h"


class ProcessMain
{
public:
    ProcessMain();

public:
    static ProcessVision* InstProcVision;
    static ProcessRecipe* InstProcRecipe;

public:
    static void InitProcess();
    static void ExitProcesses();

    // 2021.04.25 by Thienvv [ADD]
    static bool LoadRecipe();
    static bool SaveRecipe();


};

#endif // PROCESSMAIN_H
