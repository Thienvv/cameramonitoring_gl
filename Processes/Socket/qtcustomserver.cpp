#include "qtcustomserver.h"

QtCustomServer::QtCustomServer(QObject *parent):
    QTcpServer (parent)
{

}

void QtCustomServer::StartServer()
{
    int port = 5000;
    if(false == this->listen(QHostAddress::Any, port))
    {
        qDebug() << "Counld not start server!";
        return;
    }
    else
    {
        m_threadSocket = new QSocketThread(this);
        qDebug() << "Listening to port" << port << "...";
    }
}

// This function is called by QTcpServer when a new connection is avaiable
void QtCustomServer::incomingConnection(qintptr socketDescriptor)
{
    // New connection is connected
    qDebug() << socketDescriptor << "Connnecting ...";
    m_threadSocket = new QSocketThread(this);

    connect(m_threadSocket, SIGNAL(finished()), m_threadSocket, SLOT(deleteLater()));
    m_threadSocket->OnConnect(socketDescriptor);
    m_threadSocket->start();
}

bool QtCustomServer::IsRequireCapure()
{
    return m_threadSocket->IsRequireCapture();
}
bool QtCustomServer::ResponseRequireCapture()
{
    return m_threadSocket->ResponseRequireCapture();
}
