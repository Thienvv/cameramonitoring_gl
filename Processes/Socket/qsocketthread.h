#ifndef QSOCKETTHREAD_H
#define QSOCKETTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QDebug>
#include <QQueue>

class QSocketThread : public QThread
{
    Q_OBJECT

public:
    explicit QSocketThread(QObject *parent = 0);
    void run() override;

    void Send(QByteArray bytesToSend);

private:
    bool CheckCmdRequireCapture();

signals:
     void Error(QTcpSocket::SocketError socketError);

public slots:
     void OnReadyRead();
     void OnDisconnect();

private:
     QString CMD_REQUIRE_CAPTURE;

     QTcpSocket *m_Socket;
     qintptr m_SocketDescriptor;

     bool m_isRunningThread;
     int m_nSeq;

     bool m_bIsOpened;

     bool m_bIsReceivedMsg;
     QQueue<QByteArray> m_queueMsg;

     bool m_bIsRequireCapture;

private:
     QByteArray MakePackageCmdRequireCapture();
public:
     bool IsRequireCapture();
     bool ResponseRequireCapture();
     void OnConnect(qintptr ID);
};

#endif // QSOCKETTHREAD_H
