#ifndef QTCUSTOMSERVER_H
#define QTCUSTOMSERVER_H

#include <QTcpServer>
#include <Processes/Socket/qsocketthread.h>

class QtCustomServer : public QTcpServer
{
    Q_OBJECT

public:
    explicit QtCustomServer(QObject *parent = 0);

    void incomingConnection(qintptr socketDescriptor);

    void StartServer();
    bool IsRequireCapure();
    bool ResponseRequireCapture();

signals:

public slots:

protected:
    QSocketThread *m_threadSocket;

};

#endif // QTCUSTOMSERVER_H
