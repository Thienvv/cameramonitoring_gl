#include "qsocketthread.h"

QSocketThread::QSocketThread(QObject *parent) :
    QThread (parent)
{
//    this->m_SocketDescriptor = ID;

//    m_Socket = new QTcpSocket();
    m_Socket = new QTcpSocket();
    CMD_REQUIRE_CAPTURE = "RGI;01;1";
    //    CMD_REQUIRE_CAPTURE = "\002RGI;01;1\003";
    // '02 52 47 49 3b 30 31 3b 31 03'
}

void QSocketThread::OnConnect(qintptr ID)
{
//    m_Socket = new QTcpSocket();

    // Set ID
     this->m_SocketDescriptor = ID;

    if(false == m_Socket->setSocketDescriptor(this->m_SocketDescriptor))
    {
        // if something's wrong, just emit a signal
        emit Error(m_Socket->error());
        return;
    }

    // connect socket and signal
    // note - Qt::DirectConnection is used because it's multithreaded
    //        This makes the slot to be invoked immediately, when the signal is emitted.
    connect(m_Socket, SIGNAL(readyRead()), this, SLOT(OnReadyRead()), Qt::DirectConnection);
    connect(m_Socket, SIGNAL(disconnected()), this, SLOT(OnDisconnect()));

    m_bIsOpened = true;
    // We'll have multiple clients, we want to know which is which
    qDebug() << m_SocketDescriptor << " Client connected";
}

void QSocketThread::run()
{
    // Thread Start
    qDebug() << "Thread Started!";
//    m_Socket = new QTcpSocket();

//    // Set ID
//    if(false == m_Socket->setSocketDescriptor(this->m_SocketDescriptor))
//    {
//        // if something's wrong, just emit a signal
//        emit Error(m_Socket->error());
//        return;
//    }

//    // connect socket and signal
//    // note - Qt::DirectConnection is used because it's multithreaded
//    //        This makes the slot to be invoked immediately, when the signal is emitted.
//    connect(m_Socket, SIGNAL(readyRead()), this, SLOT(OnReadyRead()), Qt::DirectConnection);
//    connect(m_Socket, SIGNAL(disconnected()), this, SLOT(OnDisconnect()));
//    // We'll have multiple clients, we want to know which is which
//    qDebug() << m_SocketDescriptor << " Client connected";

    // make this thread a loop,
    // thread will stay alive so that signal/slot to function properly
    // not dropped out in the middle when thread dies

    m_nSeq = 0;
    m_isRunningThread = true;
    m_bIsRequireCapture = false;
    m_bIsReceivedMsg = false;
    exec();
    while (true)
    {
        if(false == m_isRunningThread)
            break;

        switch (m_nSeq)
        {
        case 0:
            if(false == m_bIsReceivedMsg)
            {
                break;
            }
            ++m_nSeq;
            break;

        case 1:
            m_bIsRequireCapture = CheckCmdRequireCapture();

            if(m_bIsRequireCapture)
                Send(MakePackageCmdRequireCapture());
            ++m_nSeq;
            break;

        case 2:
            ++m_nSeq;
            break;

        case 3:
            m_nSeq = 0;
            break;
        }

        QThread::msleep(1);
    }
}

void QSocketThread::OnReadyRead()
{
    // get the information
        QByteArray bytesData = m_Socket->readAll();

        // will write on server side window
        qDebug() << m_SocketDescriptor << " Data in: " << bytesData;

        if(false == m_queueMsg.contains(bytesData))
            m_queueMsg.enqueue(bytesData);

        m_bIsReceivedMsg = true;

        m_bIsRequireCapture = CheckCmdRequireCapture();
}

void QSocketThread::Send(QByteArray bytesToSend)
{
    m_Socket->write(bytesToSend);
}

void QSocketThread::OnDisconnect()
{
    qDebug() << m_SocketDescriptor << " Disconnected";
    m_Socket->deleteLater();
    m_bIsOpened = false;
    m_isRunningThread = false;

    exit(0);
}

QByteArray QSocketThread::MakePackageCmdRequireCapture()
{
    QByteArray byteData;
    byteData.append('\002');
    byteData.append(CMD_REQUIRE_CAPTURE.toUtf8());
    byteData.append('\003');
    return byteData;
}

bool QSocketThread::CheckCmdRequireCapture()
{
    QByteArray byteData = MakePackageCmdRequireCapture();
    if(false == m_queueMsg.contains(byteData)
            && false == m_queueMsg.contains(CMD_REQUIRE_CAPTURE.toUtf8()))
        return false;

    m_queueMsg.removeOne(byteData);
    m_Socket->write(byteData);
    return true;
}


bool QSocketThread::IsRequireCapture()
{
    if(false == m_bIsOpened)
        return false;

    if(false == m_bIsReceivedMsg)
        return false;

    return m_bIsRequireCapture;
}

bool QSocketThread::ResponseRequireCapture()
{
    if(m_bIsRequireCapture)
    {
//        Send(CMD_REQUIRE_CAPTURE.toUtf8());
        m_bIsRequireCapture = false;
        m_bIsReceivedMsg = false;
    }

    return true;
}
