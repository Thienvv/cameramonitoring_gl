#ifndef PROCESSVISION_H
#define PROCESSVISION_H

#include <wiringPi.h>
#include "opencv2/opencv.hpp"
#include "opencv2/highgui.hpp"
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <Defines/Definition.h>
#include <UI/ImageViewUtil/ImageViewCustom.h>
#include <Processes/ProcessBoardControl.h>
#include <Utilities/IniFile.h> // 2021.04.25 by Thienvv [ADD]
#include "Socket/qtcustomserver.h"

#include <memory>
#include <string>
#include <stdexcept>

using namespace cv;

class ProcessVision
{
    // Singleton
private:
    static ProcessVision* _instance;
    ProcessVision();



public:
    static ProcessVision* GetInstance();
    QtCustomServer MyServer;

    void StartThreadAcquisition();
    void StopThreadAcquisition();
    void StartThreadImageProcessing();
    void StopThreadImageProcessing();

    void ConvertQImageToMat(QImage qImage, Mat &matOutput);
    void ConvertMatToQImage(Mat mat, QImage &qImage);

    // 2021.04.25 by Thienvv [ADD] Save/Load Config
    bool LoadConfigFromFile(string strRecipeDir, string strRecipeName);
    bool SaveConfigToFile(string strRecipeDir, string strRecipeName);

    void DrawCrossLine();
    cv::Scalar GetGridColorValue(EnColor enColor);
    bool SaveImage(QString strFileName, QImage qImg);
    bool SaveImage(QString strFileName, Mat matImg);
    bool LoadImageFromFile(QString qstrFileName, Mat &matImg); // 2021.05.03 by Thienvv [ADD]

    bool IsCameraCapturing();
    void DoOpenCamera();
    void DoCloseCamera();
    void DoGrabImage();
    void DoImageProcessing();

    Mat GetCaptureImage();
    Mat GetDisplayImage();

public:
    static bool IsRunningThreadAcquisition;
    static bool ReqStopThreadAcquisition;
    static int CurrStepAcquisition;

    static bool IsRunningThreadImgProcessing;
    static bool ReqStopThreadImgProcessing;
    static int CurrStepImgProcessing;

    bool bShowImage;

    VideoCapture m_videoCapture;

    Mat m_matCaptured;
    Mat m_matDisplay;

    bool m_bShowGrid;
    int m_nCrossGridPitch;
    int m_nCrossGridSize;
    double m_dGridPitchDistance;
    EnColor m_enCrossCrossColor;
    EnColor m_enCrossGridColor;

    int m_nIndexDigitalInputForDelay;
    bool m_bActiveHigh;
    unsigned int m_nDelayAfterTrigger;
    bool m_bSaveImageAfterTrigger; // 2021.05.07 by Thienvv [ADD]
    int m_Illumination_Channel_1;
    int m_Illumination_Channel_2;
};
#endif // PROCESSVISION_H
