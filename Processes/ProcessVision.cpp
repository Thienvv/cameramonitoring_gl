#include "ProcessVision.h"

// Singleton
// Null, because instance will be initialized on demand.
ProcessVision* ProcessVision::_instance = nullptr;
ProcessVision* ProcessVision::GetInstance()
{
    if(_instance == nullptr)
    {
        _instance = new ProcessVision();
    }

    return _instance;
}


// Static Defined
bool ProcessVision::IsRunningThreadAcquisition = false;
bool ProcessVision::ReqStopThreadAcquisition = false;
int ProcessVision::CurrStepAcquisition = 0;
bool ProcessVision::IsRunningThreadImgProcessing = false;
bool ProcessVision::ReqStopThreadImgProcessing = false;
int ProcessVision::CurrStepImgProcessing = 0;

// Constructor
ProcessVision::ProcessVision()
{

    bShowImage = false;

    m_bShowGrid = true;
    m_nCrossGridPitch = 20;
    m_nCrossGridSize = 10;
    m_dGridPitchDistance = 1;

    m_enCrossCrossColor = COLOR_BLACK;
    m_enCrossGridColor = COLOR_RED;

    m_nIndexDigitalInputForDelay = DI_GPIO_20;
    m_bActiveHigh = false;
    m_nDelayAfterTrigger = 2000;
    m_bSaveImageAfterTrigger = true;
    m_Illumination_Channel_1 = 20;
    m_Illumination_Channel_2 = 20;

    MyServer.StartServer();

//    LoadConfigFromFile();
}

// Capture Thread
PI_THREAD (ImageAcquisition)
{
    ProcessVision *procVision = ProcessVision::GetInstance();
    while(true)
    {
        ProcessVision::IsRunningThreadAcquisition = true;
        delay(5);

        if(ProcessVision::ReqStopThreadAcquisition)
        {
            ProcessVision::IsRunningThreadAcquisition = false;
            break;
        }


        switch (ProcessVision::CurrStepAcquisition)
        {
        case 0:
            if (false == procVision->m_videoCapture.isOpened())
            {
                // cerr << "ERROR! Unable to open camera\n";
                break;
            }

            ProcessVision::CurrStepAcquisition++;
            break;

        case 1:
            procVision->DoGrabImage();
            ProcessVision::CurrStepAcquisition++;
            break;

        case 2:
//            //   procVision->GetCaptureImage();
//            procVision->DrawCrossLine();

//            if (procBoardControl->GetDIOInput(DI_SWITCH_1))
//            {
//                delay(procVision->m_nDelayAfterTrigger);
//                qDebug("Delay");
//            }
//            qDebug("Afer Delay");
            ProcessVision::CurrStepAcquisition = 1;

            break;

        case 100:
            ProcessVision::ReqStopThreadAcquisition = true;
            ProcessVision::IsRunningThreadAcquisition = false;
            break;
        }
    }
}

void ProcessVision::StartThreadAcquisition()
{
    if(ProcessVision::IsRunningThreadAcquisition == true)
        return;

    ProcessVision::ReqStopThreadAcquisition= false;
    ProcessVision::CurrStepAcquisition = 0;

    piThreadCreate (ImageAcquisition);
}

void ProcessVision::StopThreadAcquisition()
{
    ProcessVision::ReqStopThreadAcquisition = true;
    ProcessVision::CurrStepAcquisition   = 0;
}


// Image Processing Thread
PI_THREAD (ImageProcessing)
{
    ProcessVision *procVision = ProcessVision::GetInstance();
    ProcessBoardControl *procBoardControl = ProcessBoardControl::GetInstance();
    while(true)
    {
        ProcessVision::IsRunningThreadImgProcessing = true;
        delay(5);

        if(ProcessVision::ReqStopThreadImgProcessing)
        {
            ProcessVision::IsRunningThreadImgProcessing = false;
            break;
        }


        switch (ProcessVision::CurrStepImgProcessing)
        {
        case 0:
            if (false == procVision->m_videoCapture.isOpened())
            {
                // cerr << "ERROR! Unable to open camera\n";
                break;
            }

            ProcessVision::CurrStepImgProcessing++;
            break;

        case 1:
            ProcessVision::CurrStepImgProcessing++;
            break;

        case 2:
            procVision->DrawCrossLine();

        {
            // qDebug(" *** %5d ***\n", procBoardControl->GetDIOInput(procVision->m_nIndexDigitalInputForDelay));
            bool isRequireCaptureByIO = false;
            if(procVision->m_bActiveHigh == procBoardControl->GetDIOInput(procVision->m_nIndexDigitalInputForDelay))
                isRequireCaptureByIO = true;
            bool isRequireCaptureBySocket = procVision->MyServer.IsRequireCapure();

            if (isRequireCaptureByIO || isRequireCaptureBySocket)
            {
                if (true == procVision->m_bSaveImageAfterTrigger)
                {
                    string strDirData = Definition::_FOLDER_WORK_INFO;
                    QString qStrSubDir = QString("%1%2")
                            .arg(QString::fromStdString(strDirData))
                            .arg(QDate::currentDate().toString("yyyyMMdd"));
                    QDir dir(qStrSubDir);
                    if(false == dir.exists())
                        dir.mkdir(qStrSubDir);

                    QString qStrFileName = QString("%1_%2.bmp")
                            .arg(QTime::currentTime().toString("hhmmss"))
                            .arg(QTime::currentTime().msec(),3);

                    QString strFullPathFileName = QString("%1/%2")
                            .arg(qStrSubDir)
                            .arg(qStrFileName);

                    //                    QImage qImage;
                    //                    procVision->ConvertMatToQImage(procVision->m_matDisplay, qImage);
                    //                    procVision->SaveImage(strFullPathFileName,qImage);
                    //                    sudo chmod 777 filename-or-full-filepath
                    procVision->SaveImage(strFullPathFileName,procVision->m_matDisplay);
                }

                delay(procVision->m_nDelayAfterTrigger);
            }
        }
            ProcessVision::CurrStepImgProcessing++;
            break;

        case 3:

            if(procVision->MyServer.IsRequireCapure())
            {
                procVision->MyServer.ResponseRequireCapture();
            }
            ProcessVision::CurrStepImgProcessing = 1;

            break;

        case 100:
            ProcessVision::ReqStopThreadImgProcessing = true;
            ProcessVision::IsRunningThreadImgProcessing = false;
            break;
        }
    }
}

void ProcessVision::StartThreadImageProcessing()
{
    if(ProcessVision::IsRunningThreadImgProcessing == true)
        return;

    ProcessVision::ReqStopThreadImgProcessing= false;
    ProcessVision::CurrStepImgProcessing = 0;

    piThreadCreate (ImageProcessing);
}

void ProcessVision::StopThreadImageProcessing()
{
    ProcessVision::ReqStopThreadImgProcessing = true;
    ProcessVision::CurrStepImgProcessing   = 0;
}


// 2021.04.25 by Thienvv [ADD] Save/Load Config
bool ProcessVision::LoadConfigFromFile(string strRecipeDir, string strRecipeName)
{
    IniFile iniFile(strRecipeDir, strRecipeName);

    string strSection = "GRID";
    m_bShowGrid = iniFile.ReadBoolValue(strSection, "SHOW_GRID", true);
    m_nCrossGridPitch = iniFile.ReadIntValue(strSection, "GRID_PITCH", 20);
    m_nCrossGridSize = iniFile.ReadIntValue(strSection, "GRID_SIZE", 1);
    m_dGridPitchDistance = iniFile.ReadDoubleValue(strSection, "GRID_DISTANCE", 5.0);
    m_enCrossCrossColor = (EnColor)iniFile.ReadIntValue(strSection, "CROSS_COLOR", 1);
    m_enCrossGridColor = (EnColor)iniFile.ReadIntValue(strSection, "GRID_COLOR", 3);

    strSection = "ILLUMINATION";
    m_nIndexDigitalInputForDelay = iniFile.ReadIntValue(strSection, "DI_INPUT_NO", 0);
    m_bActiveHigh = iniFile.ReadBoolValue(strSection, "DI_INPUT_ACTIVE_HIGH", true);
    m_nDelayAfterTrigger = iniFile.ReadIntValue(strSection, "DELAY_AFTER_TRIGGER", 2000);
    m_bSaveImageAfterTrigger = iniFile.ReadBoolValue(strSection, "SAVE_IMAGE_AFTER_TRIGGER", true); // 2021.05.07 by Thienvv [ADD]
    m_Illumination_Channel_1 = iniFile.ReadIntValue(strSection, "ILLUMINATION_CH1", 20);
    m_Illumination_Channel_2 = iniFile.ReadIntValue(strSection, "ILLUMINATION_CH2", 20);
    return true;
}

// 2021.04.25 by Thienvv [ADD] Save/Load Config
bool ProcessVision::SaveConfigToFile(string strRecipeDir, string strRecipeName)
{
    IniFile iniFile(strRecipeDir, strRecipeName);

    string strSection = "GRID";
    iniFile.WriteBoolValue(strSection, "SHOW_GRID", m_bShowGrid);
    iniFile.WriteIntValue(strSection, "GRID_PITCH", m_nCrossGridPitch);
    iniFile.WriteIntValue(strSection, "GRID_SIZE", m_nCrossGridSize);
    iniFile.WriteDoubleValue(strSection, "GRID_DISTANCE", m_dGridPitchDistance);
    iniFile.WriteIntValue(strSection, "CROSS_COLOR", (int)m_enCrossCrossColor);
    iniFile.WriteIntValue(strSection, "GRID_COLOR", (int)m_enCrossGridColor);

    strSection = "ILLUMINATION";
    iniFile.WriteIntValue(strSection, "DI_INPUT_NO", m_nIndexDigitalInputForDelay);
    iniFile.WriteBoolValue(strSection, "DI_INPUT_ACTIVE_HIGH", m_bActiveHigh);
    iniFile.WriteIntValue(strSection, "DELAY_AFTER_TRIGGER", m_nDelayAfterTrigger);
    iniFile.WriteBoolValue(strSection, "SAVE_IMAGE_AFTER_TRIGGER", m_bSaveImageAfterTrigger);  // 2021.05.07 by Thienvv [ADD]
    iniFile.WriteIntValue(strSection, "ILLUMINATION_CH1", m_Illumination_Channel_1);
    iniFile.WriteIntValue(strSection, "ILLUMINATION_CH2", m_Illumination_Channel_2);

    return true;
}


void ProcessVision::DoOpenCamera()
{
    //--- INITIALIZE VIDEOCAPTURE
    if (m_videoCapture.isOpened())
    {
        return;
    }
    // open the default camera using default API
    // cap.open(0);
    // OR advance usage: select any API backend
    int deviceID = -1;             // 0 = open default camera
    int apiID = cv::CAP_ANY;      // 0 = autodetect default API
    // open selected camera using selected API
    m_videoCapture.open(deviceID, apiID);
    // check if we succeeded
    if (!m_videoCapture.isOpened())
    {
        cerr << "ERROR! Unable to open camera\n";
        return;
    }
}

void ProcessVision::DoCloseCamera()
{
    if (m_videoCapture.isOpened())
    {
        m_videoCapture.release();
    }
}

void ProcessVision::DoGrabImage()
{
    if (m_videoCapture.isOpened())
    {
        //        m_videoCapture.grab();
        //        int nCurrentFrame = m_videoCapture.get(cv::CAP_PROP_FRAME_COUNT);
        //        Mat tempImg;
        //        m_videoCapture.set(cv::CAP_PROP_POS_FRAMES, nCurrentFrame);
        //        m_videoCapture.retrieve(tempImg);
        //        tempImg.copyTo(m_matCaptured);
        //        tempImg.release();
        m_videoCapture.read(m_matCaptured);
    }
}

bool ProcessVision::IsCameraCapturing()
{
    return m_videoCapture.isOpened();
}

Mat ProcessVision::GetCaptureImage()
{
//    m_videoCapture.retrieve(m_matCaptured);
     return m_matCaptured;
}

Mat ProcessVision::GetDisplayImage()
{
    return m_matDisplay;
}

void ProcessVision::DoImageProcessing()
{
//    QPixmap pixmap = m_qGraphicCurrentImg->pixmap();
//    QImage image = pixmap.toImage();
//    image = image.convertToFormat(QImage::Format_RGB888);
//    cv::Mat mat = cv::Mat(
//        image.height(),
//        image.width(),
//        CV_8UC3,
//        image.bits(),
//        image.bytesPerLine());

//    cv::Mat tmp;
//    cv::blur(mat, tmp, cv::Size(8, 8));
//    mat = tmp;

//    QImage image_blurred(
//        mat.data,
//        mat.cols,
//        mat.rows,
//        mat.step,
//        QImage::Format_RGB888);
//    pixmap = QPixmap::fromImage(image_blurred);
//    m_imageScene->clear();
//    m_imageViewer->resetMatrix();
//    m_qGraphicCurrentImg = m_imageScene->addPixmap(pixmap);
//    m_imageScene->update();
//   m_imageViewer->setSceneRect(pixmap.rect());
//    QString status = QString("(editted image), %1x%2")
//            .arg(pixmap.width()).arg(pixmap.height());
//    //ui->lbl_ImageInfo->setText(status);
}


void ProcessVision::ConvertQImageToMat(QImage qImage, Mat &matOutput)
{
//    qImage = qImage.convertToFormat(QImage::Format_RGB888);
    matOutput = cv::Mat(
                qImage.height(),
                qImage.width(),
                CV_8UC3,
                qImage.bits(),
                qImage.bytesPerLine());
}

// 2021.05.07 by Thienvv Need check this function
void ProcessVision::ConvertMatToQImage(Mat mat, QImage &qImage)
{
    qImage = QImage(
                mat.data,
                mat.cols,
                mat.rows,
                mat.step,
                QImage::Format_RGB888);
}

void ProcessVision::DrawCrossLine()
{
    if (m_matCaptured.empty())
        return;

//    m_matDisplay.deallocate();
    m_matCaptured.copyTo(m_matDisplay);

    if(false == m_bShowGrid)
        return;
    cv::Point ptCenter(m_matDisplay.cols / 2, m_matDisplay.rows / 2);
    cv::Scalar crossColor = GetGridColorValue(m_enCrossCrossColor);

    int nLength = m_matDisplay.cols > m_matDisplay.rows ? m_matDisplay.cols : m_matDisplay.rows;
    int nThickness = 2;
    int nLineType = LINE_8;
    cv::drawMarker(m_matDisplay, ptCenter,  crossColor, MARKER_CROSS, nLength, nThickness, nLineType);
    //    cv::drawMarker(m_matDisplay, ptCenter,  cv::Scalar(0, 0, 0, 255), MARKER_CROSS, nLength, nThickness, nLineType);
    int nPitchX = m_nCrossGridPitch; //int(ptCenter.x / m_nCrossGridNumX);
    int nPitchY = m_nCrossGridPitch; // int(ptCenter.y / m_nCrossGridNumY);

    int nCrossGridNumX = ptCenter.x / nPitchX;
    int nCrossGridNumY = ptCenter.y / nPitchY;
    cv::Scalar gridColor = GetGridColorValue(m_enCrossGridColor);

    // Horizontal Grid
    for (int i = 0; i < nCrossGridNumX; i++)
    {
        cv::line(m_matDisplay,
                 cv::Point(ptCenter.x - nPitchX *  (i + 1), ptCenter.y - m_nCrossGridSize),
                 cv::Point(ptCenter.x - nPitchX *  (i + 1), ptCenter.y + m_nCrossGridSize),
                 gridColor, nThickness / 2, nLineType);

        cv::line(m_matDisplay,
                 cv::Point(ptCenter.x + nPitchX *  (i + 1), ptCenter.y - m_nCrossGridSize),
                 cv::Point(ptCenter.x + nPitchX *  (i + 1), ptCenter.y + m_nCrossGridSize),
                 gridColor, nThickness/ 2, nLineType);
    }

    // Vertical Grid
    for (int i = 0; i < nCrossGridNumY; i++)
    {
        cv::line(m_matDisplay,
                 cv::Point(ptCenter.x - m_nCrossGridSize, ptCenter.y - nPitchY *  (i + 1)),
                 cv::Point(ptCenter.x + m_nCrossGridSize, ptCenter.y - nPitchY *  (i + 1)),
                 gridColor, nThickness / 2, nLineType);

        cv::line(m_matDisplay,
                 cv::Point(ptCenter.x - m_nCrossGridSize, ptCenter.y + nPitchY *  (i + 1)),
                 cv::Point(ptCenter.x + m_nCrossGridSize, ptCenter.y + nPitchY *  (i + 1)),
                 gridColor, nThickness / 2, nLineType);
    }

    // Scale Text
    cv::Point ptText = cv::Point(m_matDisplay.cols - 100, m_matDisplay.rows - 30);
    int numPxOneLine = 30;
    cv::line(m_matDisplay,
             cv::Point(ptText.x, ptText.y - m_nCrossGridSize - numPxOneLine),
             cv::Point(ptText.x, ptText.y + m_nCrossGridSize - numPxOneLine),
             gridColor, nThickness / 2, nLineType);

    cv::line(m_matDisplay,
             cv::Point(ptText.x + nPitchX, ptText.y - m_nCrossGridSize - numPxOneLine),
             cv::Point(ptText.x + nPitchX, ptText.y + m_nCrossGridSize - numPxOneLine),
             gridColor, nThickness/ 2, nLineType);

    int myFontFace = 2;
    double myFontScale = 0.5;
    std::string strText = format("%.3f mm", m_dGridPitchDistance);

    cv::putText(m_matDisplay, strText, ptText, myFontFace, myFontScale, gridColor);    
}

cv::Scalar ProcessVision::GetGridColorValue(EnColor enColor)
{
    switch(enColor)
    {
    case COLOR_RED:
        return cv::Scalar(0, 0, 255, 0);
    case COLOR_GREEN:
        return cv::Scalar(0, 255, 0, 0);
    case COLOR_YELLOW:
        return cv::Scalar(0, 255, 255, 0);
    case COLOR_BLUE:
        return cv::Scalar(255, 0, 0, 0);
    case COLOR_BLACK:
        return cv::Scalar(0, 0, 0, 0);
    case COLOR_WHITE:
        return cv::Scalar(255, 255, 255, 0);
    default:
        return cv::Scalar(255, 0, 0, 0);
    }
}

bool ProcessVision::SaveImage(QString strFileName, QImage qImg)
{
    if(qImg.isNull())
        return false;

    if (!strFileName.isEmpty())
    {
        qImg.save(strFileName);
    }
}

bool ProcessVision::SaveImage(QString strFileName, Mat matImg)
{
    if (matImg.empty())
        return false;

    if (!strFileName.isEmpty())
    {
        cv::imwrite(strFileName.toStdString(), matImg);
//        cv::imwrite("strFileName.bmp", matImg);
    }
}

// 2021.05.03 by Thienvv [ADD]
bool ProcessVision::LoadImageFromFile(QString qstrFileName, Mat &matImg)
{
    std::string strFileName = TypeConversion::QStringToString(qstrFileName);
    matImg = cv::imread(strFileName, IMREAD_COLOR);
    if(matImg.empty())
        return  false;
    return true;
}


