#ifndef PROCESSRECIPE_H
#define PROCESSRECIPE_H

#include <Defines/Definition.h>
#include <Utilities/IniFile.h> // 2021.04.25 by Thienvv [ADD]
#include <stdCommon.h>

#include <memory>
#include <string>
#include <stdexcept>

using namespace std;

class ProcessRecipe
{
    // Singleton
private:
    static ProcessRecipe* _instance;
    ProcessRecipe();

private:


public:
    static ProcessRecipe* GetInstance();
//    static ProcessMain* InstProcMain;

    string CurrentRecipeDir;
    string CurrentRecipeName;

public:
    bool IsCurrentRecipeValid();
    bool LoadRecipe();
    bool SaveRecipe();
};

#endif // PROCESSRECIPE_H
