#include "ProcessRecipe.h"

// Singleton
// Null, because instance will be initialized on demand.
ProcessRecipe* ProcessRecipe::_instance = nullptr;
ProcessRecipe* ProcessRecipe::GetInstance()
{
    if(_instance == nullptr)
    {
        _instance = new ProcessRecipe();
    }

    return _instance;
}

// Constructor
ProcessRecipe::ProcessRecipe()
{
//     InstProcMain = ProcessMain::GetInstance();
//    CurrentRecipeDir = "/home/pi/Thienvv/Projects/PROGRAM/GlassMonitoring/Data/Recipes";
    CurrentRecipeDir = Definition::_FOLDER_RECIPE;
    CurrentRecipeName = "ConfigVisionProcessing.ini";
}

// Static Defined
//string ProcessRecipe::CurrentRecipeName = "";
//ProcessVision* ProcessMain::InstProcMain = nullptr;

bool ProcessRecipe::LoadRecipe()
{
    QString fileName = QFileDialog::getOpenFileName(NULL, "Load Recipe",
                                                   QString::fromStdString(CurrentRecipeDir),
                                                    "*.ini");
    QFileInfo fi(fileName);
    CurrentRecipeDir = fi.absolutePath().toStdString();
    CurrentRecipeName = fi.fileName().toStdString();

    if(CurrentRecipeName.empty())
        return false;
    return true;
}

bool ProcessRecipe::SaveRecipe()
{
    if(CurrentRecipeName.empty())
        return false;

    return true;
}

bool ProcessRecipe::IsCurrentRecipeValid()
{
    if(CurrentRecipeDir.empty() || CurrentRecipeName.empty())
        return false;

    return true;
}
