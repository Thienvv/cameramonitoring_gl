#ifndef PROCESSBOARDCONTROL_H
#define PROCESSBOARDCONTROL_H

#include "BoardController/DIOControl.h"

class ProcessBoardControl
{
    // Singleton
private:
    static ProcessBoardControl* _instance;
    ProcessBoardControl();

public:
    static DIOControl *m_DioControl;

public:
    void StartThread();
    void StopThread();

    bool SetPinModeInput(int nDioInput);
    bool GetDIOInput(EnDioInput enDioInput);
    bool GetDIOInput(int nDioInput);

public:
    static ProcessBoardControl* GetInstance();
};
#endif // PROCESSVISION_H
