#include "DIOControl.h"

DIOControl::DIOControl()
{
    mStopThreadIO = true;
    mCurrStepIO = 0;

//    for (int i = 0 ; i <NUM_INPUT; i++)
//    {
//        mDiState << i;
//    }
    InitGPIO();
}


bool DIOControl::InitGPIO()
{
    int nRet = wiringPiSetup();
    if(nRet < 0)
    {
        std::cout << "Setup failed"<<std::endl;
        return false;
    }
    else
    {
        std::cout <<" GPIO OK" <<std::endl;
    }

    pinMode(DI_GPIO_17, INPUT);
    pinMode(DI_GPIO_18, INPUT);
    pinMode(DI_GPIO_20, INPUT);
    pinMode(DI_GPIO_21, INPUT);
    pinMode(DI_GPIO_26, INPUT);

//    pinMode(LED_2, OUTPUT);
//    pinMode(LED_2, OUTPUT);
}

bool DIOControl::SetPinMode(int nDI, bool isInput)
{
    if (isInput)
    {
        pinMode(nDI, isInput);
    }
}

bool DIOControl::GetPinIn(EnDioInput enInput)
{
    return digitalRead((int)enInput);
}
bool DIOControl::GetPinIn(int nInput)
{
    return digitalRead(nInput);
}


bool DIOControl::SetPinOut(int nLed, bool bVal)
{
//    if(nLed != (int)LED_1 || nLed != (int)LED_2)
//        return false;

    if(bVal)
        digitalWrite(nLed, HIGH);
    else
        digitalWrite(nLed, LOW);
}

void DIOControl::StartThread()
{
    DIOControl::mStopThreadIO = false;
    DIOControl::mCurrStepIO= 0;
}

void DIOControl::StopThread()
{
    DIOControl::mStopThreadIO = true;
}
