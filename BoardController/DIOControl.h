#ifndef DIOCONTROL_H
#define DIOCONTROL_H

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <QList>
#include <QQueue>
#include <wiringPi.h>
#include <Defines/Definition.h>

#define NUM_INPUT 2

//#define LED_1 3
//#define LED_2 4
//#define SWITCH_1 0
//#define SWITCH_2 1


//typedef enum
//{
//  SWITCH_1 = 0,
//  SWITCH_2 = 1,
//} EnDioInput;

class DIOControl
{
public:
    DIOControl();

public:
    bool mStopThreadIO;
    bool mIsThreadRunningIO;
    int mCurrStepIO;

//public:
//    QList<bool> mDiState;


public:
    void StopThread();
    void StartThread();

    bool InitGPIO();
    bool SetPinMode(int nDI, bool isInput);

    bool SetPinOut(int nLed, bool bVal);
    bool GetPinIn(EnDioInput eInput);
    bool GetPinIn(int nInput);
};

#endif // DIOCONTROL_H
